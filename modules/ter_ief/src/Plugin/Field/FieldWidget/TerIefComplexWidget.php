<?php

namespace Drupal\ter_ief\Plugin\Field\FieldWidget;

use Drupal\taxonomy\Entity\Term;
use Drupal\termed_entity_reference\Plugin\Field\FieldWidget\TermSelectWidgetTrait;
use Drupal\inline_entity_form\Plugin\Field\FieldWidget\InlineEntityFormComplex;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Plugin implementation of the 'ter_ief_complex' widget.
 *
 * @FieldWidget(
 *   id = "ter_ief_complex",
 *   label = @Translation("Inline entity form - Complex"),
 *   description = @Translation("Inline entity form for Termed Entity Reference"),
 *   field_types = {
 *     "termed_entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class TerIefComplexWidget extends InlineEntityFormComplex {

  use TermSelectWidgetTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element += parent::formElement($items, $delta, $element, $form, $form_state);

    $element['entities']['#theme'] = 'ter_ief_entity_table';

    foreach ($items as $key => $item) {
      if (isset($element['entities'][$key])) {
        $element['entities'][$key]['#tid'] = $item->tid;
      }
    }

    if (!static::isTermElementInsertionSafeOp($form_state)) {
      return $element;
    }

    array_unshift($element['#element_validate'], [get_class($this), 'validateTermElement']);

    if (isset($element['entities'])) {
      foreach (Element::children($element['entities']) as $key) {
        if (isset($element['entities'][$key]['form'])) {
          $element['entities'][$key]['form'] += $this->formTermElement($items, $key, -1, $element, $form, $form_state);
        }
      }
    }

    if (isset($element['form'])) {
      $key = count($items);
      $element['form'] += $this->formTermElement($items, $key, -1, $element, $form, $form_state);
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareFormState(FormStateInterface $form_state, FieldItemListInterface $items, $translating = FALSE) {
    $widget_state = $form_state->get(['inline_entity_form', $this->iefId]);
    $initialize_widget_state = empty($widget_state);

    parent::prepareFormState($form_state, $items, $translating);
    $widget_state = $form_state->get(['inline_entity_form', $this->iefId]);

    if ($initialize_widget_state) {
      foreach ($items as $delta => $item) {
        $widget_state['entities'][$delta]['tid'] = $item->tid;
      }
      if (!$items->isEmpty()) {
        $form_state->set(['inline_entity_form', $this->iefId], $widget_state);
      }
    }
    elseif (static::isReferenceSaveOp($form_state) && isset($widget_state['tid']) && !empty($widget_state['entities'])) {
      $last_key = array_slice(array_keys($widget_state['entities']), -1)[0];
      $widget_state['entities'][$last_key]['tid'] = $widget_state['tid'];
      unset($widget_state['tid']);
      $form_state->set(['inline_entity_form', $this->iefId], $widget_state);
    }
    elseif (static::isWidgetStateUpdateSafeOp($form_state)) {
      foreach ($items as $delta => $item) {
        if (isset($widget_state['entities'][$delta]['tid'])) {
          $item->tid = $widget_state['entities'][$delta]['tid'];
        }
      }
      for ($delta = count($items); $delta < count($widget_state['entities']); $delta++) {
        if (isset($widget_state['entities'][$delta]['tid'])) {
          $items[] = ['tid' => $widget_state['entities'][$delta]['tid']];
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function submitSaveEntity($entity_form, FormStateInterface $form_state) {
    parent::submitSaveEntity($entity_form, $form_state);

    if (!in_array($entity_form['#op'], ['add', 'duplicate'])) {
      return;
    }

    $ief_id = $entity_form['#ief_id'];
    $tid = $form_state->get(['inline_entity_form', $ief_id, 'tid']);
    if ($tid) {
      $entities = $form_state->get(['inline_entity_form', $ief_id, 'entities']);
      $last_key = array_slice(array_keys($entities), -1)[0];
      $form_state->set(['inline_entity_form', $ief_id, 'entities', $last_key, 'tid'], $tid);
    }

  }

  public static function validateTermElement($element, FormStateInterface $form_state, $form) {
    $ief_id = $element['#ief_id'];

    if (!static::isWidgetStateUpdateSafeOp($form_state)) {
      return;
    }

    $field_name = $element['#field_name'];
    $parents = array_merge($form['#parents'], [$field_name, 'entities']);
    $submitted_values = $form_state->getValue($parents);

    foreach ($submitted_values as $key => $value) {
      if (isset($value['form']['tid'])) {
        $form_state->set(['inline_entity_form', $ief_id, 'entities', $key, 'tid'], $value['form']['tid']);
      }
    }

    $parents = array_merge($form['#parents'], [$field_name, 'form']);
    $submitted_values = $form_state->getValue($parents);
    if (isset($submitted_values['tid'])) {
      $form_state->set(['inline_entity_form', $ief_id, 'tid'], $submitted_values['tid']);
    }
  }

  protected static function getTriggeringOp(FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $op = '';
    if ($triggering_element) {
      $triggering_element_parents = $triggering_element['#parents'];
      $op = array_pop($triggering_element_parents);
    }

    return $op;
  }

  protected static function isWidgetStateUpdateSafeOp(FormStateInterface $form_state) {
    $op = static::getTriggeringOp($form_state);
    return 'ief_edit_cancel' != $op && 'ief_add_cancel' != $op && 'ief_remove_confirm' != $op;
  }

  protected static function isTermElementInsertionSafeOp(FormStateInterface $form_state) {
    $op = static::getTriggeringOp($form_state);
    return 'ief_entity_remove' != $op;
  }

  protected static function isReferenceSaveOp(FormStateInterface $form_state) {
    $op = static::getTriggeringOp($form_state);
    return 'ief_reference_save' == $op;
  }

  public static function callbackTableFieldsTerm($entity, $variables, $delta) {
    if (empty($variables['form'][$delta]['#tid'])) {
      return '';
    }

    $tid = $variables['form'][$delta]['#tid'];
    $term = Term::load($tid);
    if ($term) {
      return $term->label();
    }

    return '';
  }

}
