<?php

namespace Drupal\termed_entity_reference\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\OptGroup;

trait TermSelectWidgetTrait {

  public function formTermElement(FieldItemListInterface $items, $delta, $weight, array $element, array &$form, FormStateInterface $form_state) {

    $options = ['_none' => t('- Select a value -')];
    $options += OptGroup::flattenOptions($this->getTermSelectionHandler()->getReferenceableEntities());

    $term_element = [
      '#type' => 'select',
      '#title' => t('Reference term'),
      '#required' => TRUE,
      '#multiple' => FALSE,
      '#delta' => $delta,
      '#weight' => $weight,
      '#options' => $options,
      '#default_value' => $this->getTermSelectedOption($items, $delta, $options),
    ];

    return ['tid' => $term_element];
  }

  protected function getTermSelectedOption($items, $delta, $options) {
    $selected_option = '_none';
    if (isset($items[$delta])) {
      $value = $items[$delta]->tid;
      if (isset($options[$value])) {
        $selected_option = $value;
      }
    }

    return $selected_option;
  }

  protected function getTermSelectionHandler() {
    return \Drupal::service('plugin.manager.entity_reference_selection')
      ->getSelectionHandler($this->getTermFieldDefinition());
  }

  protected function getTermFieldDefinition() {
    $field_definition = $this->fieldDefinition;
    $vocabulary = $field_definition->getFieldStorageDefinition()->getSetting('vocabulary');
    $settings = [
      'target_type' => 'taxonomy_term',
      'cardinality' => 1,
      'handler' => 'default',
      'handler_settings' => [
        'target_bundles' => [$vocabulary => $vocabulary],
      ],
    ];

    $term_field_definition = BaseFieldDefinition::create('entity_reference')
      ->setComputed(TRUE)
      ->setSettings($settings);

    return $term_field_definition;
  }
}
