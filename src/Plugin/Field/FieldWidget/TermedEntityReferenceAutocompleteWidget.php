<?php

namespace Drupal\termed_entity_reference\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'termed_entity_reference_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "termed_entity_reference_autocomplete",
 *   label = @Translation("Autocomplete"),
 *   description = @Translation("An autocomplete text field."),
 *   field_types = {
 *     "termed_entity_reference"
 *   }
 * )
 */
class TermedEntityReferenceAutocompleteWidget extends EntityReferenceAutocompleteWidget {

  use TermSelectWidgetTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element += parent::formElement($items, $delta, $element, $form, $form_state);
    $element += $this->formTermElement($items, $delta, $delta, $element, $form, $form_state);

    if (!isset($element['#element_validate'])) {
      $element['#element_validate'] = [];
    }
    $element['#element_validate'][] = [get_class($this), 'validateTermElement'];

    return $element;
  }

  public static function validateTermElement(array $element, FormStateInterface $form_state) {
    if ($element['tid']['#required'] && $element['tid']['#value'] == '_none' && !empty($element['target_id']['#value'])) {
      $form_state->setError($element, t('@name field is required.', ['@name' => $element['tid']['#title']]));
    }
  }

}
