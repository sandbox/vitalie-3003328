<?php

namespace Drupal\termed_entity_reference\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'options_select' widget.
 *
 * @FieldWidget(
 *   id = "termed_entity_reference_options_buttons",
 *   label = @Translation("Check boxes/radio buttons"),
 *   field_types = {
 *     "termed_entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class TermedEntityReferenceOptionsButtonsWidget extends OptionsButtonsWidget {

  use TermSelectWidgetTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element += parent::formElement($items, $delta, $element, $form, $form_state);
    $element += $this->formTermElement($items, $delta, $delta, $element, $form, $form_state);

    return $element;
  }

}
