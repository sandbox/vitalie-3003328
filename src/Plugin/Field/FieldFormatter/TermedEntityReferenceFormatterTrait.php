<?php

namespace Drupal\termed_entity_reference\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;

/**
 * Trait to override some methods of EntityReferenceFormatterBase.
 */
trait TermedEntityReferenceFormatterTrait {

  /**
   * @see EntityReferenceFormatterBase
   */
  public function prepareView(array $entities_items) {
    parent::prepareView($entities_items);

    $term_ids = [];
    $term_entities = [];

    foreach ($entities_items as $items) {
      foreach ($items as $item) {
        $item->_term_loaded = FALSE;
        if (!$item->hasNewTerm()) {
          $term_ids[] = $item->tid;
        }
      }
    }
    if ($term_ids) {
      $term_entities = \Drupal::entityManager()->getStorage('taxonomy_term')->loadMultiple($term_ids);
    }

    foreach ($entities_items as $items) {
      foreach ($items as $item) {
        if (isset($term_entities[$item->tid])) {
          $item->term = $term_entities[$item->tid];
          $item->_loaded = TRUE;
        }
        elseif ($item->hasNewTerm()) {
          $item->_loaded = TRUE;
        }
      }
    }
  }

  /**
   * @see EntityReferenceFormatterBase
   */
  protected function getEntitiesToView(EntityReferenceFieldItemListInterface $items, $langcode) {
    $entities = parent::getEntitiesToView($items, $langcode);

    foreach ($entities as $delta => $entity) {
      $term = $entity->_referringItem->term;
      if ($term instanceof TranslatableInterface) {
        $term = \Drupal::entityManager()
          ->getTranslationFromContext($term, $langcode);
      }

      if (!$term) {
        \Drupal::logger('termed_entity_reference')->notice('Stale or missing term reference on entity @label', ['@label' => $items->getParent()->getValue()->label()]);
      }

      $entity->_referenceTerm = $term;
    }

    return $entities;
  }

}
