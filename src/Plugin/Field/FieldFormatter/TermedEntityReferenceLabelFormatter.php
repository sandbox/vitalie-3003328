<?php

namespace Drupal\termed_entity_reference\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;

/**
 * Plugin implementation of the 'termed entity reference label' formatter.
 *
 * @FieldFormatter(
 *   id = "termed_entity_reference_label",
 *   label = @Translation("Label"),
 *   description = @Translation("Display the label of the referenced entities and the associated term name."),
 *   field_types = {
 *     "termed_entity_reference"
 *   }
 * )
 */
class TermedEntityReferenceLabelFormatter extends EntityReferenceLabelFormatter {

  use TermedEntityReferenceFormatterTrait;

  const POS_AFTER = 'after';
  const POS_BEFORE = 'before';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'term_position' => self::POS_AFTER,
        'term_prefix' => ' - ',
        'term_suffix' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['term_position'] = [
      '#title' => t('Term position'),
      '#type' => 'select',
      '#options' => $this->getTermPositionOptions(),
      '#default_value' => $this->getSetting('term_position'),
    ];

    $elements['term_prefix'] = [
      '#title' => t('Term prefix'),
      '#type' => 'textfield',
      '#size' => 10,
      '#default_value' => $this->getSetting('term_prefix'),
    ];

    $elements['term_suffix'] = [
      '#title' => t('Term suffix'),
      '#type' => 'textfield',
      '#size' => 10,
      '#default_value' => $this->getSetting('term_suffix'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $position_options = $this->getTermPositionOptions();
    $position = $this->getSetting('term_position');
    if (isset($position_options[$position])) {
      $summary[] = t('Term position') . ': ' . $position_options[$position];
    }

    $summary[] = t('Term prefix') . ': ' .$this->getSetting('term_prefix');
    $summary[] = t('Term suffix') . ': ' .$this->getSetting('term_suffix');

    return $summary;
  }

  protected function getTermPositionOptions() {
    return [
      self::POS_BEFORE => t('Before'),
      self::POS_AFTER => t('After'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $output_as_link = $this->getSetting('link');

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $label = $entity->label();

      if ($output_as_link && !$entity->isNew()) {
        try {
          $uri = $entity->urlInfo();
        }
        catch (UndefinedLinkTemplateException $e) {
          $output_as_link = FALSE;
        }
      }

      $reference_term_label = isset($entity->_referenceTerm) ? $entity->_referenceTerm->label() : '';
      $term_label = Html::escape($this->getSetting('term_prefix') . $reference_term_label . $this->getSetting('term_suffix'));

      if ($output_as_link && isset($uri) && !$entity->isNew()) {
        $elements[$delta] = [
          '#type' => 'link',
          '#title' => $label,
          '#url' => $uri,
          '#options' => $uri->getOptions(),
        ];

        if (!empty($items[$delta]->_attributes)) {
          $elements[$delta]['#options'] += ['attributes' => []];
          $elements[$delta]['#options']['attributes'] += $items[$delta]->_attributes;
          unset($items[$delta]->_attributes);
        }

        if ($this->getSetting('term_position') === self::POS_BEFORE) {
          $elements[$delta]['#prefix'] = $term_label;
        }
        else {
          $elements[$delta]['#suffix'] = $term_label;
        }
      }
      else {
        if ($this->getSetting('term_position') === self::POS_BEFORE) {
          $label = $term_label . $label;
        }
        else {
          $label .= $term_label;
        }
        $elements[$delta] = ['#plain_text' => $label];
      }
      $elements[$delta]['#cache']['tags'] = $entity->getCacheTags();
    }

    return $elements;
  }

}
