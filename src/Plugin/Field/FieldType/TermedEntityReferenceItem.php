<?php

namespace Drupal\termed_entity_reference\Plugin\Field\FieldType;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;


/**
 * Defines the 'termed_entity_reference' entity field type.
 *
 * Supported settings (below the definition's 'settings' key) are:
 * - exclude_entity_types: Allow user to include or exclude entity_types.
 * - entity_type_ids: The entity type ids that can or cannot be referenced.
 *
 * @property int target_id
 * @property int tid
 * @property \Drupal\Core\Entity\ContentEntityInterface entity
 *
 * @FieldType(
 *   id = "termed_entity_reference",
 *   label = @Translation("Termed entity reference"),
 *   description = @Translation("An entity field extending entity reference to allow a term."),
 *   category = @Translation("Termed Entity Reference"),
 *   default_widget = "termed_entity_reference_autocomplete",
 *   default_formatter = "termed_entity_reference_label",
 *   list_class = "\Drupal\termed_entity_reference\Plugin\Field\TermedEntityReferenceFieldItemList"
 * )
 */
class TermedEntityReferenceItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
        'vocabulary' => '',
      ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $settings = $field_definition->getSettings();
    $term_target_type = 'taxonomy_term';
    $term_type_info = \Drupal::entityManager()
      ->getDefinition($term_target_type);
    $properties = parent::propertyDefinitions($field_definition);

    $tid_definition = DataReferenceTargetDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('@label ID', ['@label' => $term_type_info->getLabel()]))
      ->setSetting('unsigned', TRUE)->setRequired(TRUE);
    $properties['tid'] = $tid_definition;

    $properties['term'] = DataReferenceDefinition::create('entity')
      ->setLabel($term_type_info->getLabel())
      ->setDescription(new TranslatableMarkup('The term describing the reference'))
      ->setComputed(TRUE)
      ->setReadOnly(FALSE)
      ->setTargetDefinition(EntityDataDefinition::create($term_target_type))
      ->addConstraint('EntityType', $term_target_type);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['tid'] = [
      'description' => 'The ID of the term.',
      'type' => 'int',
      'unsigned' => TRUE,
    ];

    $schema['indexes']['tid_target_id'] = ['tid', 'target_id'];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $target = FALSE;
    $term = FALSE;

    if ($this->target_id !== NULL) {
      $target = TRUE;
    }
    if (!$target && $this->entity && $this->entity instanceof EntityInterface) {
      $target = TRUE;
    }

    if (!$target) {
      return TRUE;
    }

    if ($this->tid !== NULL) {
      $term = TRUE;
    }
    if (!$term && $this->term && $this->term instanceof EntityInterface) {
      $term = TRUE;
    }

    return !$term;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    if (isset($values) && !is_array($values)) {
      throw new \InvalidArgumentException('Invalid values given. Values must be represented as an associative array.');
    }
    else {
      parent::setValue($values, FALSE);

      // See parent code, as we replicate it in case of tid/term.
      if (is_array($values) && array_key_exists('tid', $values) && !isset($values['term'])) {
        $this->onChange('tid', FALSE);
      }
      elseif (is_array($values) && !array_key_exists('tid', $values) && isset($values['term'])) {
        $this->onChange('term', FALSE);
      }
      elseif (is_array($values) && array_key_exists('tid', $values) && isset($values['term'])) {
        $tid = $this->get('term')->getTargetIdentifier();
        if (!$this->term->isNew() && $values['tid'] !== NULL && ($tid != $values['tid'])) {
          throw new \InvalidArgumentException('The tid and term passed to the entity reference item do not match.');
        }
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $values = parent::getValue();

    if ($this->hasNewTerm()) {
      $values['term'] = $this->term;
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($property_name, $notify = TRUE) {
    if ($property_name == 'term') {
      $property = $this->get('term');
      $tid = $property->isTargetNew() ? NULL : $property->getTargetIdentifier();
      $this->writePropertyValue('tid', $tid);
    }
    elseif ($property_name == 'tid') {
      $this->writePropertyValue('term', $this->tid);
    }
    parent::onChange($property_name, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();

    if ($this->hasNewTerm()) {
      if ($this->term->isNew()) {
        $this->term->save();
      }

      $this->tid = $this->term->id();
    }

    if (!$this->isEmpty() && $this->tid === NULL && $this->term) {
      $this->tid = $this->term->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values = parent::generateSampleValue($field_definition);

    //        $term_field_settings = [
    //            'target_type' => 'taxonomy_term',
    //            'handler' => $field_definition->getSetting('handler'),
    //            'handler_settings' => $field_definition->getSetting('handler_settings'),
    //        ];
    //        $term_field_definition = BaseFieldDefinition::create('entity_reference')->setSettings($term_field_settings);
    //
    //        $term_values = parent::generateSampleValue($term_field_definition);
    //
    //        if ($term_values) {
    //            $values['tid'] = $term_values['target_id'];
    //            $values['term'] = $term_values['entity'];
    //        }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $bundles = \Drupal::entityManager()->getBundleInfo('taxonomy_term');
    $bundle_options = [];
    foreach ($bundles as $bundle_name => $bundle_info) {
      $bundle_options[$bundle_name] = $bundle_info['label'];
    }
    natsort($bundle_options);

    $element['vocabulary'] = [
      '#type' => 'select',
      '#title' => t('Vocabulary'),
      '#options' => $bundle_options,
      '#default_value' => $this->getSetting('vocabulary'),
      '#required' => TRUE,
      '#disabled' => $has_data,
      '#size' => 1,
    ];

    return $element;
  }

  /**
   * Determines whether the item holds an unsaved term.
   *
   * This is notably used for "autocreate" widgets, and more generally to
   * support referencing freshly created terms (they will get saved
   * automatically as the hosting entity gets saved).
   *
   * @return bool
   *   TRUE if the item holds an unsaved entity.
   */
  public function hasNewTerm() {
    return !$this->isEmpty() && $this->tid === NULL && $this->term && $this->term->isNew();
  }

  /**
   * {@inheritdoc}
   */
  public static function calculateDependencies(FieldDefinitionInterface $field_definition) {
    $dependencies = parent::calculateDependencies($field_definition);
    $entity_manager = \Drupal::entityManager();
    $term_entity_type = $entity_manager->getDefinition('taxonomy_term');

    if ($default_value = $field_definition->getDefaultValueLiteral()) {
      foreach ($default_value as $value) {
        if (is_array($value) && isset($value['term_uuid'])) {
          $entity = $entity_manager->loadEntityByUuid($term_entity_type->id(), $value['term_uuid']);
          if ($entity) {
            $dependencies[$term_entity_type->getConfigDependencyKey()][] = $entity->getConfigDependencyName();
          }
        }
      }
    }

    $vocabulary = $field_definition->getFieldStorageDefinition()->getSetting('vocabulary');
    $storage = $entity_manager->getStorage($term_entity_type->getBundleEntityType());
    if ($storage && $bundle = $storage->load($vocabulary)) {
      $dependencies[$bundle->getConfigDependencyKey()][] = $bundle->getConfigDependencyName();
    }

    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public static function calculateStorageDependencies(FieldStorageDefinitionInterface $field_definition) {
    $dependencies = parent::calculateStorageDependencies($field_definition);
    $term_entity_type = \Drupal::entityManager()
      ->getDefinition('taxonomy_term');
    $dependencies['module'][] = $term_entity_type->getProvider();
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public static function onDependencyRemoval(FieldDefinitionInterface $field_definition, array $dependencies) {
    $changed = parent::onDependencyRemoval($field_definition, $dependencies);
    $entity_manager = \Drupal::entityManager();
    $term_entity_type = $entity_manager->getDefinition('taxonomy_term');

    if ($default_value = $field_definition->getDefaultValueLiteral()) {
      foreach ($default_value as $key => $value) {
        if (is_array($value) && isset($value['term_uuid'])) {
          $entity = $entity_manager->loadEntityByUuid($term_entity_type->id(), $value['term_uuid']);

          if ($entity && isset($dependencies[$entity->getConfigDependencyKey()][$entity->getConfigDependencyName()])) {
            unset($default_value[$key]);
            $changed = TRUE;
          }
        }
      }
      if ($changed) {
        $field_definition->setDefaultValue($default_value);
      }
    }

    return $changed;
  }

  /**
   * {@inheritdoc}
   */
  public static function getPreconfiguredOptions() {
    return [];
  }

}
