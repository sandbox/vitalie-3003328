<?php

namespace Drupal\termed_entity_reference\Plugin\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a item list class for entity reference fields.
 */
class TermedEntityReferenceFieldItemList extends EntityReferenceFieldItemList {

  /**
   * {@inheritdoc}
   */
  public static function processDefaultValue($default_value, FieldableEntityInterface $entity, FieldDefinitionInterface $definition) {
    $default_value = parent::processDefaultValue($default_value, $entity, $definition);

    if ($default_value) {
      $uuids = [];
      foreach ($default_value as $delta => $properties) {
        if (isset($properties['term_uuid'])) {
          $uuids[$delta] = $properties['term_uuid'];
        }
      }
      if ($uuids) {
        $entity_ids = \Drupal::entityQuery('taxonomy_term')
          ->condition('uuid', $uuids, 'IN')
          ->execute();
        $entities = \Drupal::entityManager()
          ->getStorage('taxonomy_term')
          ->loadMultiple($entity_ids);

        $entity_uuids = [];
        foreach ($entities as $id => $entity) {
          $entity_uuids[$entity->uuid()] = $id;
        }
        foreach ($uuids as $delta => $uuid) {
          if (isset($entity_uuids[$uuid])) {
            $default_value[$delta]['tid'] = $entity_uuids[$uuid];
            unset($default_value[$delta]['term_uuid']);
          }
          else {
            unset($default_value[$delta]);
          }
        }
      }

      $default_value = array_values($default_value);
    }
    return $default_value;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultValuesFormSubmit(array $element, array &$form, FormStateInterface $form_state) {
    $default_value = parent::defaultValuesFormSubmit($element, $form, $form_state);

    $ids = [];
    foreach ($default_value as $delta => $properties) {
      if (isset($properties['term']) && $properties['term']->isNew()) {
        $properties['term']->save();
        $default_value[$delta]['tid'] = $properties['term']->id();
        unset($default_value[$delta]['term']);
      }
      $ids[] = $default_value[$delta]['tid'];
    }
    $entities = \Drupal::entityManager()
      ->getStorage('taxonomy_term')
      ->loadMultiple($ids);

    foreach ($default_value as $delta => $properties) {
      unset($default_value[$delta]['tid']);
      $default_value[$delta]['term_uuid'] = $entities[$properties['tid']]->uuid();
    }
    return $default_value;
  }

}
